from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        'todos': todos
    }
    return render(request, 'todos/list.html', context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        'todo_detail': todo,
    }
    return render(request, 'todos/detail.html', context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, 'todos/create.html', context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoForm(instance=todo)
    context = {
        'todo_object': todo,
        'form': form,
    }
    return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/delete.html')


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, 'todos/create_item.html', context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        'form': form,
    }
    return render(request, 'todos/edit_item.html', context)
